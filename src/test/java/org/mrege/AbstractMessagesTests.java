package org.mrege;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.mrege.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import com.ibm.icu.text.MessageFormat;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.Fail.fail;

@SpringBootTest
abstract class AbstractMessagesTests {

	private static final String COMMAND_PREFIX = "command.";
	private static final String INFO_COMMAND_PREFIX = "info.";
	private static final String COMMAND_SYNTAX_PREFIX = "syntax.";

	private static final String SLASH = "/";
	private static final String SEPARATOR_COMMANDS = " - ";

	private static final Logger log = LoggerFactory.getLogger(AbstractMessagesTests.class);

	@Autowired
	@Qualifier("telegramMessages")
	List<String> telegramMessages;

	@Autowired
	@Qualifier("telegramCommands")
	List<String> telegramCommands;

	@Autowired
	@Qualifier("entities")
	Map<String, Map<String, Object>> entities;

	public String getMessageBaseName () {
		return "messages";
	}

	public String getTelegramBaseName () {
		return "telegram";
	}

	public Locale getLocale() {
		return Locale.forLanguageTag(getLanguage());
	}

	protected abstract String getLanguage();

	public String getLabel (String baseName, String label) {
		ResourceBundle bundle = ResourceBundle.getBundle(baseName, getLocale());
		return  bundle.getString(label);
	}

	@Test
	void existsTelegramMessages() {
		for (String label: telegramMessages) {
			assertThat(getLabel(getMessageBaseName(), label)).isNotEmpty();
		}
	}

	@Test
	void existsTelegramCommands() {
		for (String label: telegramCommands) {
			assertThat(getLabel(getTelegramBaseName(), COMMAND_PREFIX+label)).isNotEmpty();
			assertThat(getLabel(getTelegramBaseName(), INFO_COMMAND_PREFIX+label)).isNotEmpty();
			assertThat(getLabel(getTelegramBaseName(), COMMAND_SYNTAX_PREFIX+label)).isNotEmpty();
		}
	}

	String formatMessage(String pattern, Map<String, Object> variables) {
		log.debug("Replace pattern {}, with variables {}", pattern, variables);
		MessageFormat formatter = new MessageFormat(pattern, getLocale());
		return formatter.format(variables);
	}


	@SneakyThrows
	@Test
	void checkVariablesInMessages() {
		Map<String, List<String>> labels = new ObjectMapper().readValue(
				FileUtils.getResource("/labels.json"),
				HashMap.class);
		log.debug("Labels {}", labels);
		for (Map.Entry<String, List<String>> entry: labels.entrySet()) {
			log.debug("Key {}, value {}", entry.getKey(), entry.getValue());
			Map<String, Object> variables = getVariables(entry.getValue());
			String pattern = getLabel(getMessageBaseName(), entry.getKey());
			String message = formatMessage(pattern, variables);
			log.debug("Final message for label {}: {}", entry.getKey(), message);
			Pattern regex = Pattern.compile("(\\{[\\w+-]+\\})");
			Matcher matcher = regex.matcher(message);

			List<String> variablesWithoutReplace = new ArrayList<>();

			while (matcher.find()) {
				variablesWithoutReplace.add(matcher.group(1));
			}

			if (!variablesWithoutReplace.isEmpty()) {
				fail(String.format("%d variables without replacements in message label: [%s], variables: %s, final message: %s",
						variablesWithoutReplace.size(), entry.getKey(), variablesWithoutReplace, message));
			}
		}
	}

	private Map<String, Object> getVariables(List<String> entities) {
		Map<String, Object> variables = new HashMap<>();
		for (String entity: entities) {
			variables.putAll(this.entities.get(entity));
		}

		return variables;
	}

	@Test
	public void createCommandsDescription () throws IOException {
		File tempFile = File.createTempFile("commands-", "-"+getLanguage()+".tmp");
		try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tempFile)))) {
			for (String command: telegramCommands) {
				writer.write(SLASH);
				writer.write(getLabel(getTelegramBaseName(), COMMAND_PREFIX+command));
				writer.write(SEPARATOR_COMMANDS);
				writer.write(getLabel(getTelegramBaseName(), INFO_COMMAND_PREFIX+command));
				writer.newLine();
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		log.info("File {}", tempFile);
	}

}
