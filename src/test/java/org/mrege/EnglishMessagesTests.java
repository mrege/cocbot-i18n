package org.mrege;

class EnglishMessagesTests extends AbstractMessagesTests{

	@Override
	protected String getLanguage() {
		return "en";
	}
}
