package org.mrege;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CocbotI18nApplication {
    public static void main(String[] args) {
    	SpringApplication.run(CocbotI18nApplication.class, args);
    }

}
