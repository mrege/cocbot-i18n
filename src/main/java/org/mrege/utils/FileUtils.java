package org.mrege.utils;

import io.micrometer.core.instrument.util.IOUtils;

import java.io.InputStream;
import java.nio.charset.Charset;

public class FileUtils {

    public static String getResource(String classpathLocation) {
        return IOUtils.toString(org.apache.commons.io.FileUtils.class.getResourceAsStream(classpathLocation),
                Charset.defaultCharset());
    }

    public static String getResource(InputStream inputStream) {
        return IOUtils.toString(inputStream);
    }
}
