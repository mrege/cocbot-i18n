package org.mrege.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.mrege.utils.FileUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class AppConfig {

    @Bean(name="telegramMessages")
    public List<String> getTelegramMessages() {
        return List.of("ability.commands.notFound",
                "ability.recover.success",
                "ability.recover.fail",
                "ability.recover.message",
                "ability.recover.error",
                "ability.ban.success",
                "ability.ban.fail",
                "ability.unban.success",
                "ability.unban.fail",
                "ability.promote.success",
                "ability.promote.fail",
                "ability.demote.success",
                "ability.demote.fail",
                "ability.claim.success",
                "ability.claim.fail",
                "checkInput.fail",
                "checkLocality.fail",
                "checkPrivacy.fail",
                "userNotFound");
    }

    @Bean(name="telegramCommands")
    public List<String> getTelegramCommands() {
        return List.of("myclan",
                "start",
                "setattack",
                "delattack",
                "delete_assignments",
                "infowar",
                "infoleague",
                "opponent",
                "autoassign",
                "assignments",
                "about",
                "attacks",
                "pendingattacks",
                "poll",
                "getpoll",
                "getresultpoll",
                "closepoll",
                "delpool",
                "playerconfig",
                "lottery",
                "myclans",
                "cleancaches",
                "syncwithclan",
                "infoclans",
                "lastattacks",
                "setplayer",
                "setdefaultclan",
                "delclan",
                "penalties",
                "accounts",
                "mypendingattacks",
                "delaccount",
                "settings",
                "help");
    }

    @SneakyThrows
    @Bean(name="entities")
    public Map<String, Map<String, Object>> getEntities() {
        Map<String, Map<String, Object>> entities = new HashMap<>();
        ClassLoader cl = this.getClass().getClassLoader();
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(cl);
        Resource[] resources = resolver.getResources("classpath:entities/*.json") ;
        for (Resource resource: resources){
            entities.put(getNameOfEntity(resource),
                    new ObjectMapper().readValue(
                            FileUtils.getResource(resource.getInputStream()),
                    HashMap.class));
        }

        return entities;
    }

    private String getNameOfEntity(Resource resource) throws IOException {
        return resource.getFile()
                .getName()
                .replace(".json", "");
    }


}
