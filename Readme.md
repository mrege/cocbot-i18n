# Getting Started

Send an MR to add a change

## Run a tests

<code>./mvnw test</code>

The tests check that all available labels exist and that there are no undefined variables.

The variables of each label can have are defined [here](src/main/resources/labels.json) and each entity with its variables is defined [here](src/main/resources/entities).

To add a new language you must create a class equal to [this](src/test/java/org/mrege/EnglishMessagesTests.java) and override de method <code> getLanguage </code> with the location.

And create the files <code>src/main/resources/messages_{location}.properties</code> and <code>src/main/resources/telegram_{location}.properties</code> and 
